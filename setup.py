import codecs
from setuptools import setup


with codecs.open('README.rst', encoding='utf-8') as f:
    long_description = f.read()

setup(
    name = "gitx",
    version = '0.5',
    license='http://www.apache.org/licenses/LICENSE-2.0',
    description= 'A simple tool that help you speed up git clone',
    author = 'yunba',
    author_email= 'liuliqiu@yunba.io',
    packages = ['client'],
    install_requires = [],
    entry_points = """
    [console_scripts]
    git = client.gitx:main
    """,
    long_description = long_description,
)