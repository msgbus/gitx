#!/usr/bin/env python
#coding:utf-8
from log_analyzer import log_analyzied_cluster
from db_operation import squid_db

def update_quota_left_all(log_path,bak_log_path):
    db = squid_db('121.42.140.2',3306,'squid','yunba','yunba')
    conn = db.connect_squid()
    network_data = log_analyzied_cluster(log_path,bak_log_path)
    for (user,bytes) in network_data.items():
        db.update_quota_left(conn,user,bytes,'sub')
    print 'update quota_left success'

def add_user(userinfo):
    db = squid_db('121.42.140.2', 3306, 'squid', 'yunba', 'yunba')
    conn = db.connect_squid()
    db.add_user(conn,userinfo)

def update_user_time(user,newtime):
    db = squid_db('121.42.140.2', 3306, 'squid', 'yunba', 'yunba')
    conn = db.connect_squid()
    db.update_time(conn,user,newtime)

def update_quota_left_user(user,bytes):
    db = squid_db('121.42.140.2', 3306, 'squid', 'yunba', 'yunba')
    conn = db.connect_squid()
    db.update_quota_left(conn,user,bytes,'add')
    print 'update success'


def update_user_other_info(user,info):
    db = squid_db('121.42.140.2', 3306, 'squid', 'yunba', 'yunba')
    conn = db.connect_squid()
    db.update_user_info(conn,user,info)

if __name__ == '__main__':
    log_path = '/var/log/squid/access.log'
    bak_log_path = '/var/log/squid/access.log.1'
    update_quota_left_all(log_path,bak_log_path)

#    userinfo = {'user':'liuliqiu','password':'liuliqiu','enabled':1, \
#                'quota':1000000000,'quota_left':1000000000,'fullname':'liuliqiu','comment':'liuliqiu'}
#    add_user(userinfo)

#    user = 'testuser'
#    newtime = '2016-09-10 08:00:00'
#    update_user_time(user,newtime)

#    user = 'test'
#    bytes = 100000000
#    update_quota_left_user(user,bytes)

#    user = 'test'
#    info = {'comment':'liuliqiu1','fullname':'liuliqiu1'}
#    update_user_other_info(user,info)


