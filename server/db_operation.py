#!/usr/bin/env python
#coding:utf-8
import MySQLdb as sdb

class squid_db():
    def __init__(self,host,port,db,user,passwd):
        self.host = host
        self.port = port
        self.db = db
        self.user = user
        self.passwd = passwd

    def connect_squid(self):
        try:
            conn = sdb.connect(self.host,self.user,self.passwd,self.db,self.port)
            return conn
        except Exception,e:
            print e
            exit(1)

    def query(self,conn,keys):
        cur = conn.cursor()
        strtemp = "select * from passwd where "
        for (key,value) in keys.items():
            strtemp = strtemp + key + "='" + str(value)+"' and "
        cmd = strtemp.rstrip('and ')
        try:
            cur.execute(cmd)
            data = cur.fetchall()
            if data:
                return data
            else:
                print 'query success but no such user'
        except Exception,e:
            print e

    def add_user(self,conn,userinfo):
        cmd = "insert into passwd set "
        for (key,value) in userinfo.items():
            cmd = cmd + key+"='"+str(value)+"',"
        cmd = cmd.rstrip(',')+';'
        cur = conn.cursor()
        try:
            cur.execute(cmd)
            conn.commit()
            print 'add user success'
        except Exception,e:
            print e
            exit(1)

    def update_quota_left(self,conn,user,bytes,select):
        try:
            cur = conn.cursor()
            cur.execute("select quota_left,quota from passwd where user = %s and (quota_left is not null) for update",(user,))
            data = cur.fetchall()
            if data:
                quota_left = int(data[0][0])
                quota_total = int(data[0][1])
                if select == 'sub':
                    quota_left = quota_left - bytes
                else:
                    quota_left = quota_left + bytes
                    quota_total = quota_total + bytes
                    cur.execute("update passwd set quota = %s where user = %s",(str(quota_total),user))
                if quota_left > 0:
                    cur.execute("update passwd set quota_left = %s , enabled = 1 where user = %s and time is null", (str(quota_left),user,))
                else:
                    cur.execute("update passwd set quota_left = %s , enabled = 0 where user = %s and time is null and enabled = 1", (str(quota_left), user,))
                cur.execute("update passwd set quota_left = %s where user = %s and time is not null and quota is not null",(str(quota_left), user,))
            conn.commit()
        except Exception,e:
            print e
            exit(1)

    def update_time(self,conn,user,newtime):
        cur = conn.cursor()
        try:
            cur.execute("update passwd set time = %s , enabled = 1 where user = %s",(newtime,user,))
            conn.commit()
            print 'udpate time success'
        except Exception,e:
            print e
            exit(1)

    def update_user_info(self,conn,user,info):
        cmd = "update passwd set "
        str = ""
        for (key,value) in info.items():
            str =str + key + "='"+value+"',"
        cmd = cmd + str.rstrip(',')+" where user = '%s'"%user
        cur = conn.cursor()
        try:
            cur.execute(cmd)
            conn.commit()
            print 'udpate user information success'
        except Exception,e:
            print e
            exit(1)

if __name__ == '__main__':
    db = squid_db('121.42.140.2', 3306, 'squid', 'yunba', 'yunba')
    conn = db.connect_squid()
    print db.query(conn,{'user':'test','enabled':1})