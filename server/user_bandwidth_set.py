#!/usr/bin/env python
#coding:utf-8
import os
import linecache

def add_user_in_limit_list(user,config_path):
    try:
        config = linecache.getlines(config_path)
        for i in range(0,len(config)):
            if 'acl speedlimit' in config[i]:
                config[i] = config[i].rstrip('\n')+' ' +user +'\n'
                break
        with open(config_path,'w+') as f:
            f.writelines(config)
    except Exception,e:
        print e
        exit(1)

def delete_user_in_limit_list(user,config_path):
    data = linecache.getlines(config_path)
    for i in range(0,len(data)):
        if 'acl speedlimit' in data[i] and user in data[i]:
            list = data[i].strip('\n').split()
            list.remove(user)
            data[i] = ' '.join(list)+'\n'
            break
    try:
        with open(config_path,'w+') as f:
            f.writelines(data)
    except Exception,e:
        print e
        exit(1)

def restart_squid():
    cmd = 'sudo /usr/sbin/squid -k reconfigure'
    try:
        os.system(cmd)
    except Exception,e:
        print e
        exit(1)

if __name__ == '__main__':
    add_user_in_limit_list('liuliqiu','squid.conf')