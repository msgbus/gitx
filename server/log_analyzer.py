#!/usr/bin/env python
#coding:utf-8
import os

def log_analyzied_one_user(path,username):
    totalbytes = 0
    with open(path) as f:
        for line in f:
            str = line.split()
            if str[7] == username:
                totalbytes += int(str[4])
    return totalbytes

def log_analyzied_all_user(path,dict):
    if os.path.isfile(path) and os.path.getsize(path) != 0:
        with open(path) as f:
            for line in f:
                str = line.split()
                username = str[7]
                bytes = int(str[4])
                if username in dict.keys():
                    dict[username] += bytes
                else:
                    dict[username] = bytes
        if '-' in dict.keys():
            dict.pop('-')
        return dict
    else:
        return {}


def log_analyzied_cluster(log_path,bak_log_path):
    if os.path.isfile(log_path) and os.path.getsize(log_path) != 0:
        try:
            flog = open(log_path,'r+')
        except Exception,e:
            print e
            exit(1)
        data = flog.readlines()
        flog.truncate(0)
        flog.close()
        dict = {}
        for line in data:
            str = line.split()
            username = str[7]
            bytes = int(str[4])
            if username in dict.keys():
                dict[username] += bytes
            else:
                dict[username] = bytes
        try:
            fbak = open(bak_log_path, 'a')
        except Exception,e:
            print e
            exit(1)
        fbak.writelines(data)
        fbak.close()
        if '-' in dict.keys():
            dict.pop('-')
        return dict
    else:
        return {}

if __name__ == '__main__':
    path = '/var/log/squid/access.log'
    print log_analyzied_all_user(path,{})
#    print log_analyzied_cluster('/var/log/squid/access.log', '/var/log/squid/access.log.1')
