#!/usr/bin/env python
#coding:utf-8
import random

import nose

import log_analyzer
import db_operation
import user_bandwidth_set
import quota_update

def test_log_analyzied_one_user():
    assert log_analyzer.log_analyzied_one_user('access.log','test') != 0
    assert log_analyzer.log_analyzied_one_user('/home/acces.log','test') == 0
    assert log_analyzer.log_analyzied_one_user('access.log','lsifsl') == 0

def test_log_analyzied_all_user():
    assert log_analyzer.log_analyzied_all_user('access.log',{}) != {}
    assert log_analyzer.log_analyzied_all_user('/home/access.log',{}) == {}

def test_log_analyzied_cluster():
    assert log_analyzer.log_analyzied_cluster('access.log', 'access.log.1') != {}
    assert log_analyzer.log_analyzied_cluster('/home/access.log', 'access.log.1') == {}

def test_connect_squid():
    db = db_operation.squid_db('121.42.140.2', 3306, 'squid', 'yunba', 'yunba')
    assert db.connect_squid() != None

def test_query():
    db = db_operation.squid_db('121.42.140.2', 3306, 'squid', 'yunba', 'yunba')
    conn = db.connect_squid()
    assert db.query(conn, {'user': 'test', 'enabled': 1}) != None
    assert db.query(conn, {'user':'jskf', 'enabled':1}) == None

def test_add_user():
    db = db_operation.squid_db('121.42.140.2', 3306, 'squid', 'yunba', 'yunba')
    conn = db.connect_squid()
    db.add_user(conn,{'user':'testliuliqiu','password':'test','enabled':'1'})

def test_update_quota_left():
    db = db_operation.squid_db('121.42.140.2', 3306, 'squid', 'yunba', 'yunba')
    conn = db.connect_squid()
    db.update_quota_left(conn,'test',100,'sub')
    db.update_quota_left(conn,'test',100,'add')

def test_update_time():
    db = db_operation.squid_db('121.42.140.2', 3306, 'squid', 'yunba', 'yunba')
    conn = db.connect_squid()
    db.update_time(conn,'testuser','2017-01-01 15:00:00')

def test_update_user_info():
    db = db_operation.squid_db('121.42.140.2', 3306, 'squid', 'yunba', 'yunba')
    conn = db.connect_squid()
    db.update_user_info(conn,'testuser',{'comment':'liuliqiu'})

def test_restart_squid():
    user_bandwidth_set.restart_squid()

def test_add_user_in_limit_list():
    user_bandwidth_set.add_user_in_limit_list('liuliqiu','squid.conf')

def test_delete_user_in_limit_list():
    user_bandwidth_set.delete_user_in_limit_list('liuliqiu','squid.conf')

def test_udpate_quota_left_all():
    log_path = 'access.log'
    bak_log_path = 'access.log.1'
    quota_update.update_quota_left_all(log_path, bak_log_path)

def test_add_user():
    number = random.randint(1,10000)
    userinfo = {'user': 'liuliqiu11'+str(number), 'password': 'liuliqiu', 'enabled': 1, \
                            'quota':1000000000,'quota_left':1000000000,'fullname':'liuliqiu','comment':'liuliqiu'}
    quota_update.add_user(userinfo)

def test_update_user_time():
    user = 'testuser'
    newtime = '2018-09-10 08:00:00'
    quota_update.update_user_time(user,newtime)

def test_update_quota_left_user():
    user = 'test'
    bytes = 100000000
    quota_update.update_quota_left_user(user,bytes)

def test_update_user_other_info():

    user = 'test'
    info = {'comment':'liuliqiu2','fullname':'liuliqiu2'}
    quota_update.update_user_other_info(user,info)

if __name__ == '__main__':
    nose.runmodule()