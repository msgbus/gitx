# 多平台安装pip
--------------
 
##ubuntu，debian

`sudo apt-get install python-pip`

##centos，redhat

首先安装epel扩展源：

`sudo yum install epel-release`

然后安装python-pip：

`sudo yum install python-pip`

##Mac OS

Mac OS默认是不带pip的，通过执行以下命令安装：

`sudo easy_install pip`

##Others

请自行查找相关资料
