gitx
===========

|PyPI version| |Build Status| |Coverage Status|

A simple tool that help you speed up git clone.

Client
------

Install
~~~~~~~

Debian / Ubuntu:

::

    sudo apt-get install python-pip
    sudo pip install gitx

CentOS:

::

    sudo yum install python-setuptools && easy_install pip
    sudo pip install gitx

Usage
~~~~~

To init the tool with proxy account and appkey:

::

    git proxy_set username your_username appkey your_appkey

Clone some respository:

::

    git clone https://github.com/jankari/test

To see the gitx version:
::
    git -v
