#gitx 使用说明
----------------------

[gitx.me静态网页](http://120.24.183.72/)

[代理服务器列表地址](http://120.24.183.72/proxy_list)

-----------------------

##注意

  1.gitx使用git完成所有的操作，每次运行前都会检测git是否安装，未安装则会提示安装；

  2.gitx运行时自动从上面的代理服务器列表地址获取代理服务器列表并任意选择其中一个作为本次clone的代理服务器，如果获取失败则会使用内置在工具中的默认代理服务器；
  
  3.暂时只支持github和bitbucket clone、pull和push加速，自动将github和bitbucket的ssh协议转换成https协议，bitbucket使用ssh协议时需要输入用户名和密码。
  
##安装（需要sudo权限）

    
```
#!shell

sudo pip install gitx

git proxy_set username yunbaproxy appkey Yunba1702
```

     
  帐号密码的配置文件在：`～/.gitxconfig`
   
##使用方法
  1.设置代理账号密码
  
    git proxy_set username your_username appkey your_appkey
  
  2.支持所有的原生git命令，clone github和bitbucket的项目以及push和pull仓库时自动使用代理，clone完成后自动取消代理设置。

##功能介绍
  1.版本查看
   
    git --version
 