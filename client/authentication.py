#!/usr/bin/env python
#coding:utf-8
import os
import urllib2
import random

def get_username_appkey(config_path=os.environ['HOME'] + '/.gitxconfig'):
    if os.path.isfile(config_path):
        try:
            with open(config_path) as f:
                for line in f:
                    data = line.strip('\n').split(':')
                    if data[0] == 'username':
                        username = data[1]
                    if data[0] == 'appkey':
                        appkey = data[1]
            if username and appkey:
                return username + ':' + appkey
            else:
                print 'Please check your gitx config file in correct form!'
        except Exception,e:
            print e
    else:
        print 'There are seem to be no config file,if you want to use proxy speed please init the gitx!'
    return 0

def get_proxy_server_list(proxy_url='http://120.24.183.72/proxy_list'):
    try:
        list = urllib2.urlopen(proxy_url)
        proxy_address = list.readlines()
        real_proxy = proxy_address[random.randint(0,len(proxy_address)-1)].rstrip('\n')
    except Exception,e:
        print 'Get remote proxy server list fail,more error information:'
        print e
        print 'Use the default proxy server instead'
        real_proxy = 'f2.yunba.io:1708'
    return real_proxy

def auth_for_appkey(username_appkey,proxy_server):
    if username_appkey:
        proxy = 'http://'+username_appkey+'@'+proxy_server
        proxies = { "http":proxy, "https": proxy, }
        try:
            proxy_handler = urllib2.ProxyHandler(proxies=proxies)
            opener = urllib2.build_opener(proxy_handler, urllib2.HTTPHandler)
            content = opener.open('https://github.com')
            if content.code == 200:
                return proxy
        except Exception,e:
            print 'Auth failed,more error information:'
            print e
    return 0

if __name__ == '__main__':
    print get_proxy_server_list()