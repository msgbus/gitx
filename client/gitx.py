#!/usr/bin/env python
#coding:utf-8
import sys
import os
import commands

import proxy
import git
from authentication import auth_for_appkey
from authentication import get_username_appkey
from authentication import get_proxy_server_list

def main():
    git.find_git()
    argv = sys.argv
    if len(argv) < 2:
        git.git_operation(argv)

    elif 'pull' == argv[1] or 'push' == argv[1]:
        proxy_address = auth_for_appkey(get_username_appkey(), get_proxy_server_list())
        if proxy_address:
            proxy.set_proxy(proxy_address)
        try:
            git.git_operation(argv)
        except Exception, e:
            print e
        if proxy_address:
            os.system('/usr/bin/git config --global --unset http.proxy')

    elif 'proxy_set' == argv[1]:
        if len(argv) == 6:
            git.git_init(argv[3], argv[5])
        else:
            print 'Proxy username or appkey set error!'
            exit(1)

    elif len(argv) == 2:
        if argv[1] == '--version':
            status,out = commands.getstatusoutput('/usr/bin/git --version')
            print 'gitx version 0.5 (based on ' +out+')'
        else:
            git.git_operation(argv)

    elif 'clone' == argv[1]:
        address_list = argv[2:]
        address = None
        for temp in address_list:
            if not temp.startswith(r'-'):
                address = temp
                break

        if address == None:
            git.git_operation(argv)

        elif address.startswith(r'https://github.com') or address.startswith(r'git@github.com') or address.startswith(r'git://github.com') or (address.startswith(r"https://") and ("@bitbucket.org" in address)):
            proxy_address = auth_for_appkey(get_username_appkey(),get_proxy_server_list())
            if proxy_address:
                proxy.set_proxy(proxy_address)
            try:
                git.git_operation(argv)
            except Exception,e:
                print e
            if proxy_address:
                os.system('/usr/bin/git config --global --unset http.proxy')

        elif address.startswith(r'git@bitbucket.org:'):
            proxy_address = auth_for_appkey(get_username_appkey(),get_proxy_server_list())
            if proxy_address:
                proxy.set_proxy(proxy_address)
            username = raw_input('Please input your bitbucket username:')
            command = r'/usr/bin/git clone https://' + username + '@bitbucket.org/'+ address.split(':')[1]
            try:
                os.system(command)
            except Exception,e:
                print e
            if proxy_address:
                os.system('/usr/bin/git config --global --unset http.proxy')

        else:
            git.git_operation(argv)

    else:
        git.git_operation(argv)

if __name__ == '__main__':
    main()
