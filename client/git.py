#!/usr/bin/env python
#coding:utf-8
import commands
import os
import platform

def git_init(username,appkey,config_path = os.environ['HOME'] + '/.gitxconfig'):
    try:
        with open(config_path,'w') as f:
            f.write('username:'+username+'\n')
            f.write('appkey:'+appkey+'\n')
    except Exception,e:
        print e
        exit(1)

def find_git(git_path=None):
    if git_path:
        status, out = commands.getstatusoutput('ls -l ' + git_path)
    else:
        status1, out = commands.getstatusoutput('ls -l /usr/bin/git')
        status2, out = commands.getstatusoutput('ls -l /usr/libexec/git-core/git')
        status3, out = commands.getstatusoutput('ls -l /usr/lib/git-core/git')

    if status1 and status2 and status3:
        print 'There seem to be not git on your system,please install git first!'
        select = raw_input('Do you want to install git now(yes/no):')
        if select.lower() == 'yes':
            osname = platform.platform().lower()
            if ('debian' in osname) or ('ubuntu' in osname):
                os.system('sudo apt-get install -y --reinstall git')
            elif ('centos' in osname) or ('redhat' in osname):
                os.system('sudo yum reinstall -y git')
            elif 'darwin' in osname:
                os.system('sudo brew install git')
            else:
                print "Can't find proper package for your system,please install by yourself!"
                exit(1)
        else:
            exit(1)
    elif status1 and not (status2 and status3):
        if not status2:
            os.system('sudo cp /usr/libexec/git-core/git /usr/bin/git')
        elif not status3:
            os.system('sudo cp /usr/lib/git-core/git /usr/bin/git')
    else:
        return

def git_operation(argv):
    str = '/usr/bin/git '
    for i in range(1,len(argv)):
        str = str + '\"' + argv[i] + '\" '
    try:
        os.system(str)
    except Exception,e:
        print e

if __name__ == '__main__':
    find_git()

