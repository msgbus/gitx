#squid3安装配置说明
------------------

##1.squid3安装:
  
`sudo apt-get update`

`sudo apt-get install squid3`

##2.检查squid3的数据库验证插件[basic_db_auth](http://manpages.ubuntu.com/manpages/trusty/man8/basic_db_auth.8.html)是否安装成功:

`sudo ls -la /usr/lib/squid3/basic_db_auth`

##3.安装DBD和DBI插件连接远程数据库(使用远程数据库验证账号密码时需要):

`sudo apt-get install libdbd-mysql-perl`

`sudo apt-get install libdbi-perl`

##4.检查DBD和DBI是否安装成功(需要安装perldoc:`sudo apt-get install perldoc`):

`perldoc DBI`

`perldoc DBD::mysql`

##5.安装mysql数据库(本地或者远端均可):

`sudo apt-get install mysql-server`

##6.修改mysql配置文件`/etc/mysql/my.cnf`允许远程访问(需要远程访问mysql数据库时):

将`bind-address           = 127.0.0.1`一行注释掉

##7.创建mysql数据库和passwd表保存用户名和密码:

```
#!mysql
mysql> create database squid;

```

##

##8.设置允许yunba用户从任意地址访问squid数据库:

`mysql> GRANT ALL PRIVILEGES ON *.* TO 'yunba'@'%' IDENTIFIED BY 'yunba' WITH GRANT OPTION;`

##9.创建passwd表保存用户名和密码:

```
#!shell

mysql> CREATE TABLE `passwd` (
  `user` varchar(32) NOT NULL default '',
  `password` varchar(35) NOT NULL default '',
  `enabled` tinyint(1) NOT NULL default '1',
  `fullname` varchar(60) default NULL,
  `comment` varchar(60) default NULL,
  PRIMARY KEY  (`user`)
);
```

##10.添加一个测试用户:

`mysql> insert into passwd values('testuser','test',1,'Test User','for testing purpose');`


##11.更新squid配置文件`/etc/squid3/squid3.conf`,添加下面的内容:

###mysql安装在本地时:

```
#!shell

auth_param basic program /usr/lib/squid3/basic_db_auth \
    --user yunba --password yunba --plaintext --persist

auth_param basic children 5
auth_param basic realm Web-Proxy
auth_param basic credentialsttl 1 minute
auth_param basic casesensitive off

acl db-auth proxy_auth REQUIRED
http_access allow db-auth
http_access allow localhost
http_access deny all


```

###mysql安装在远端时:

```
#!shell

auth_param basic program /usr/lib/squid3/basic_db_auth \
    --dsn "DBI:mysql:host=121.42.140.2;port=3306;database=squid" \
    --user yunba --password yunba --plaintext --persist

auth_param basic children 5
auth_param basic realm Web-Proxy
auth_param basic credentialsttl 1 minute
auth_param basic casesensitive off

acl db-auth proxy_auth REQUIRED
http_access allow db-auth
http_access allow localhost
http_access deny all

```

##12.重启squid3:

`sudo service squid3 restart`

##13.浏览器设置代理验证