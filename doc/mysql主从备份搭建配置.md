#mysql master/slave安装配置说明
------------------

##1.检测master和slave所在机器能否相互ping通

`master ip:192.168.1.126`

`slave ip:192.168.1.145`

##2.master端安装配置：

(1).安装mysql-server：

  `sudo apt-get install mysql-server`

(2).修改配置文件`/etc/mysql/my.cnf`
  
  注释 `bind-address           = 127.0.0.1`行

(3).在[mysqld]的下面加入下面代码：

  
```
#!shell

  log-bin=mysql-bin

  server-id=1

  innodb_flush_log_at_trx_commit=1

  sync_binlog=1

  binlog_do_db=weibo

  binlog_ignore_db=mysql
```

说明：

1)     log-bin=mysql-bin启用Binary Log；

2)     server-id=1中的1可以任定义，只要是唯一的就行；

3)     innodb_flush_log_at_trx_commit=1

4)     sync_binlog=1

5)     binlog_do_db=weibo是表示只备份weibo；（binlog-do-db）

6)     binlog_ignore_db=mysql表示忽略备份mysql；

不加binlog_do_db和binlog_ignore_db，那就表示备份全部数据库。

(4).重启mysql:

  `sudo service mysql restart`

(5).登录mysql服务器
`mysql –u root -p`

在主服务器新建一个用户赋予“REPLICATION SLAVE”的权限。你不需要再赋予其它的权限。

`mysql>CREATE USER yunba@'192.168.1.145' IDENTIFIED BY 'yunba';`

`mysql>GRANT REPLICATION SLAVE ON *.* TO yunba@'192.168.1.145' IDENTIFIED BY 'yunba';`

`mysql>FLUSH PRIVILEGES;`

(6).查看master状态:

`mysql>SHOW MASTER logs;`

`mysql>SHOW MASTER STATUS;`

记下File及Position的值,后面做从服务器操作的时候需要用.

+------------------+----------+--------------+------------------+

| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB |

+------------------+----------+--------------+------------------+

| mysql-bin.000002 |      107 | weibo        | mysql            |

+------------------+----------+--------------+------------------+

##3.slave端安装配置:

(1).安装mysql-server：

  `sudo apt-get install mysql-server`

(2).修改配置文件`/etc/mysql/my.cnf`
  
  注释 `bind-address           = 127.0.0.1`行

(3).在[mysqld]的下面加入下面代码：

`server-id=2`

(4).重启mysql:

`sudo service mysql restart`

(5).登录MySQL服务器:

`mysql -u root -p`

执行以下命令：

```
#!mysql

mysql>CHANGE MASTER TO

MASTER_HOST='192.168.1.126',

MASTER_USER='yunba',

MASTER_PASSWORD='yunba',

MASTER_PORT=3306,

MASTER_LOG_FILE='mysql-bin.000002',

MASTER_LOG_POS=107,

MASTER_CONNECT_RETRY=10;
```

说明：

MASTER_HOST:主服务器的IP；

MASTER_USER：配置主服务器时建立的用户名；

ASTER_PASSWORD：用户密码；

ASTER_PORT：主服务器mysql端口，如果未曾修改，默认即可。

(6).启动Slave进程

`mysql>START SLAVE;`

(7).主从同步检查

`mysql>show slave status\G;`

其中`Slave_IO_Running` 与 `Slave_SQL_Running` 的值都必须为YES，才表明状态正常。\G的好处就是以下图的格式进行显示，\g是没有这个效果的。

##4.测试搭建结果：

连接master执行创建数据：

`create database yunba`

连接slave执行查询数据库操作：

`show databases`

如果发现存在yunba数据库说明搭建成功！