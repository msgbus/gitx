#gitx原型设计文档
##1.项目目的
    
  - 提高git clone的速度。
  
##2.项目模块
    
  - 底层使用git完成操作，需要安装git才可以使用这个工具，支持所有的git命令操作，增加代理账号设置命令，自动创建代理配置文件。

```
#!shell

    gitx proxy_set username your_proxy_username appkey your_proxy_appkey
```

  - 主模块（gitx.py）：解析gitx命令行参数，按照命令行参数进行相关的操作；
  - 验证模块（authentication.py）：验证用户名和appkey是否正确并返回用户个人的代理地址；
  - 代理模块（proxy.py)：根据验证模块返回的代理地址设置git代理；
  - git实际操作模块（git.py）：根据命令行参数执行具体git操作。
    
##3.gitx程序流程图

![gitx程序流程图.jpg](https://bitbucket.org/repo/nLbKyE/images/98565086-gitx%E7%A8%8B%E5%BA%8F%E6%B5%81%E7%A8%8B%E5%9B%BE.jpg)
  
##4.项目目标

  - 实现git加速功能；
  - 实现用户管理和付费管理；
  - 实现快速安装，配置。

##5.项目说明

  - 用户验证直接在代理服务器squid完成；
  - 使用mysql数据库存储用户名，appkey以及过期时间；
  - 工具付费方式：按时间、按流量、按带宽。

#6.整体架构图

  - 用户少：

![整体架构图-小.png](https://bitbucket.org/repo/nLbKyE/images/66177587-%E6%95%B4%E4%BD%93%E6%9E%B6%E6%9E%84%E5%9B%BE-%E5%B0%8F.png)

  

  - 用户多：

![整体架构图-大.png](https://bitbucket.org/repo/nLbKyE/images/3353754875-%E6%95%B4%E4%BD%93%E6%9E%B6%E6%9E%84%E5%9B%BE-%E5%A4%A7.png)
